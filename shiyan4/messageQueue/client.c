#include<stdio.h>
 #include<unistd.h>
 #include<stdlib.h>
 #include<string.h>
 #include<errno.h>
 #include<sys/msg.h>
 #include<sys/ipc.h>

 #define IPC_KEY 0x123123
  //定义消息队列的key，好知道写道那个队列中，取哪个队列中拿
  #define TYPE_R 1
  #define TYPE_W 2

  struct msgbuf
  {
      long mtype;
      char mtext[1024];
  };
  int main()
  {
      int msqid = -1;
      msqid = msgget(IPC_KEY,IPC_CREAT|0664);
      if(msqid < 0)
      {
          perror("msgget error");
          return -1;
      }
      while(1)
      {
          struct msgbuf buf;
       
          memset(&buf,0,sizeof(struct msgbuf));
          msgrcv(msqid,&buf,1024,TYPE_W,0);
          printf("w say:[%s]\n",buf.mtext);
      
          memset(&buf,0,sizeof(struct msgbuf));
          buf.mtype = TYPE_R;
          printf(">>>");
          scanf("%s",buf.mtext);
          msgsnd(msqid,&buf,1024,0);
      }
      msgctl(msqid,IPC_RMID,NULL);
      return 0;
  }
