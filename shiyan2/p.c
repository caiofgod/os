#include "pv.h"
int p(int semid)
{
  struct sembuf p_buf;

  p_buf.sem_num=0;
  p_buf.sem_op=-1;        //信号量减1，注意这一行的1前面有个负号
  p_buf.sem_flg=SEM_UNDO;
  
  //p_buf = {0,-1,SEM_UNDO};
  if (semop(semid, &p_buf, 1)==-1)   
  {
    perror("p(semid)failed");
    exit(1);
  }
  return(0);
}
