
#include "pv.h"
extern int p();
extern int v();
extern int initsem();
extern int fork();

 int main()
{
  key_t semkey_A=0x200;
  key_t semkey_B=0x220;
  key_t semkey_c=0x240;
  int semid_A,semid_B,semid_c;
  if ((semid_A=initsem(semkey_A,1))<0) exit(1);
  if ((semid_B=initsem(semkey_B,0))<0) exit(1);
  if ((semid_c=initsem(semkey_c,0))<0) exit(1);
  printf("A   进程A的信号量标识符%d,它的初始值为%d\n",
           semid_A,semctl(semid_A, 0, GETVAL)); 
  printf("B   进程B的信号量标识符%d,它的初始值为%d\n",
           semid_B,semctl(semid_B, 0, GETVAL)); 
    printf("c   进程c的信号量标识符%d,它的初始值为%d\n",
           semid_c,semctl(semid_c, 0, GETVAL)); 
          
  int pid1=fork();
  int pid2 = fork();      
if(pid1>0&&pid2>0){
 

    for(int i =0;i<20; i++){

       
        
        p(semid_A);printf("父亲放了一个苹果\n");v(semid_c);
    }
}
else if(pid1>0&&pid2==0){
  
    for(int i =0;i<5 ;i++){
        
    
        p(semid_A);printf("母亲放了一个橘子\n");v(semid_B);
    }
    

}
else if (pid1==0&&pid2>0){
  
    for(int i =0;i<20 ;i++)
   { 

        p(semid_B);printf("儿子吃一个橘子\n");v(semid_A);
   }
}
else
{
   
    for(int i =0;i<5 ;i++){
         
   
        p(semid_c);
    printf("女儿吃一个苹果\n");
    v(semid_A);


    }
    
}
return 0;
}
